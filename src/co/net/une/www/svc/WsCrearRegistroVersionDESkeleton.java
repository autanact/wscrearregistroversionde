
/**
 * WsCrearRegistroVersionDESkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsCrearRegistroVersionDESkeleton java skeleton for the axisService
     */
    public class WsCrearRegistroVersionDESkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsCrearRegistroVersionDELocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param codigoDireccion
                                     * @param nombreArchivo
         */
        

                 public co.net.une.www.gis.WsCrearRegistroVersionDERSType crearRegistroVersionDE
                  (
                  co.net.une.www.gis.BoundedString100 codigoDireccion,co.net.une.www.gis.BoundedString100 nombreArchivo
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("codigoDireccion",codigoDireccion);params.put("nombreArchivo",nombreArchivo);
		try{
		
			return (co.net.une.www.gis.WsCrearRegistroVersionDERSType)
			this.makeStructuredRequest(serviceName, "crearRegistroVersionDE", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    